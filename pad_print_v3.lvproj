﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="13008000">
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{02BE4CAA-3676-4C0B-B19A-C8796640FBF4}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Inputs-AIN.VALUE</Property>
	<Property Name="varPersistentID:{02DD5178-05FB-4AA8-9F2A-6948BBC2C9D3}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO21</Property>
	<Property Name="varPersistentID:{09D99EC0-0332-4BBA-A579-A05F79813FC7}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO30</Property>
	<Property Name="varPersistentID:{0AD973D3-9568-4B31-84D4-2FF640335A74}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI12</Property>
	<Property Name="varPersistentID:{0DCECF64-85B6-4D1C-8810-232DD2C1E7B7}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Inputs-Status word</Property>
	<Property Name="varPersistentID:{114F8D90-A0D0-40AE-85E6-5A2F34D178E9}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Outputs-ClearDigInputChangedBit</Property>
	<Property Name="varPersistentID:{1852357E-22F3-4572-A0F5-4E3D81E84BD4}" Type="Ref">/NI-cRIO9074-Padprint/Pad_Print_v3.lvlib/network_variables/Printing No</Property>
	<Property Name="varPersistentID:{1BBAB95B-67E4-4610-9EEB-EBD196FA8AFB}" Type="Ref">/NI-cRIO9074-Padprint/Pad_Print_v3.lvlib/network_variables/variables</Property>
	<Property Name="varPersistentID:{220E387E-F626-4D2D-8A22-DCF03B9B41F5}" Type="Ref">/My Computer/Dependencies/Pad_Print_v3.lvlib/network_variables/Variable sensor</Property>
	<Property Name="varPersistentID:{2479361D-F62C-46B6-A5F0-121432CDE71A}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Outputs-Digital Outputs</Property>
	<Property Name="varPersistentID:{250C6D20-4B10-4474-9650-99C00B87371A}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI13</Property>
	<Property Name="varPersistentID:{27016ADA-3416-4B49-9776-66832FC5B686}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Inputs-Following Error Actual Value</Property>
	<Property Name="varPersistentID:{29A6F45F-1DD9-46DA-8DAF-5EDFCC1B958A}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI3</Property>
	<Property Name="varPersistentID:{2A63B48E-FDD6-46FB-B931-97D4BEF3141F}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Inputs-Latch 1P</Property>
	<Property Name="varPersistentID:{2AD8CF6B-510A-428F-B841-3C28BE01B98F}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI19</Property>
	<Property Name="varPersistentID:{2BF834E9-47D0-42F3-956A-E5698E4FB9A3}" Type="Ref">/My Computer/Dependencies/Pad_Print_v3.lvlib/network_variables/time cliche</Property>
	<Property Name="varPersistentID:{2C452689-5CFB-4A0A-B9EE-AE482B3FAE43}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI7</Property>
	<Property Name="varPersistentID:{3522A8AE-D27A-4048-B5B3-F7B79244D534}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO17</Property>
	<Property Name="varPersistentID:{388D8822-20B7-400E-905E-054BA33D42CB}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI26</Property>
	<Property Name="varPersistentID:{3B64EB12-1DE5-47B3-A81C-E04348747F2D}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO16</Property>
	<Property Name="varPersistentID:{40357205-4386-4132-B9CF-F085B4EC09E9}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI27</Property>
	<Property Name="varPersistentID:{415A66B8-DE68-42C8-84F3-E3BFB5EF76D9}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO29</Property>
	<Property Name="varPersistentID:{41F56A00-5803-4744-8315-A4E58FB4B3D7}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Inputs-AIN.VALUE</Property>
	<Property Name="varPersistentID:{44AEB2AE-1284-4B46-83B0-878511805990}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod4/AI1</Property>
	<Property Name="varPersistentID:{4740E182-E269-44FF-9B24-E47C6CE57FAC}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Inputs-Status word</Property>
	<Property Name="varPersistentID:{4770E37A-B5B3-479C-9F1E-28BD9E93F906}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO28</Property>
	<Property Name="varPersistentID:{48C79339-D6BB-4B51-9700-C24718FD0FA5}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI14</Property>
	<Property Name="varPersistentID:{49B81869-196D-47A0-9FBF-879746E00E4D}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI30</Property>
	<Property Name="varPersistentID:{4BA1C323-93B2-4275-B0EE-5CB206F7C3CB}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI23</Property>
	<Property Name="varPersistentID:{4D1F7A56-0355-4F36-BD93-4089F0024088}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI6</Property>
	<Property Name="varPersistentID:{516AFE31-14F9-4230-AA26-DF2901E7D67D}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI2</Property>
	<Property Name="varPersistentID:{53EBFC5A-7BA4-4AFF-8699-6BFBE47E444C}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Inputs-Position Actual Internal Value</Property>
	<Property Name="varPersistentID:{5603AB9D-E520-4E18-B2F8-583BDE234936}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO26</Property>
	<Property Name="varPersistentID:{57BBC68D-84E4-415D-B9E9-846066B665F9}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Outputs-Latch Control Word</Property>
	<Property Name="varPersistentID:{5B85D935-ECA3-4441-B8CD-DC35637E4563}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI31</Property>
	<Property Name="varPersistentID:{5C7646A2-646B-416A-BFC8-EB5F6F91B582}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Outputs-ClearDigInputChangedBit</Property>
	<Property Name="varPersistentID:{5F7EE94A-13B0-4904-B75E-6AB58CA94DB3}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI18</Property>
	<Property Name="varPersistentID:{60B43627-22A6-4B65-B35E-027EBCAB8E3F}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO14</Property>
	<Property Name="varPersistentID:{62E1A486-5E04-4388-8E19-25D58C044CAE}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Inputs-Velocity Actual Value</Property>
	<Property Name="varPersistentID:{638EBD0C-DB43-4033-A930-705705039D8F}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Outputs-Control word</Property>
	<Property Name="varPersistentID:{67E5BF3E-B63A-4D47-A4C9-49B4E640F562}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Outputs-Latch Control Word</Property>
	<Property Name="varPersistentID:{6BBB95AF-F6C4-49E4-A931-71C7B1272CF4}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Inputs-Digital Inputs</Property>
	<Property Name="varPersistentID:{6CDBA6BE-31D3-44B0-907B-E2CE19B7614A}" Type="Ref">/My Computer/Dependencies/Pad_Print_v3.lvlib/network_variables/Printing No</Property>
	<Property Name="varPersistentID:{6E0C0187-4E94-485C-A0EF-A569456835D4}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO0</Property>
	<Property Name="varPersistentID:{76173A0B-6E61-49CB-A7A4-9BCDC78DC2B2}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI29</Property>
	<Property Name="varPersistentID:{777341C7-A986-4E00-ABE9-55BF959BBE34}" Type="Ref">/NI-cRIO9074-Padprint/Pad_Print_v3.lvlib/network_variables/main prog cont switch</Property>
	<Property Name="varPersistentID:{7779CA4D-C3DF-4AAE-B186-50A98D75C502}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO4</Property>
	<Property Name="varPersistentID:{78BCE9EA-EB96-4A3E-8B9D-F9ED2BEB8DD1}" Type="Ref">/NI-cRIO9074-Padprint/Pad_Print_v3.lvlib/network_variables/Variable sensor</Property>
	<Property Name="varPersistentID:{79BF1D1C-2BB8-43B6-9546-3BC2F6D2E805}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI21</Property>
	<Property Name="varPersistentID:{7A080FC1-9996-4C24-B205-8DBE6401A7CE}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO31</Property>
	<Property Name="varPersistentID:{7AD8E245-2BE1-45A7-8D9E-57EA4865F271}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO15</Property>
	<Property Name="varPersistentID:{7EF7F22A-CD09-4353-B17A-6EA628E5C03C}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO6</Property>
	<Property Name="varPersistentID:{7F15808A-542D-40CA-B90B-F62AAA7C7610}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI0</Property>
	<Property Name="varPersistentID:{7FF77B40-89B1-4F7B-AE12-B3EE4DEAE217}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod4/AI0</Property>
	<Property Name="varPersistentID:{830FF972-A245-4951-98F2-25446BE3D14B}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Inputs-Following Error Actual Value</Property>
	<Property Name="varPersistentID:{83BC69B9-8BEB-4473-9021-AFA7F8242CB1}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI11</Property>
	<Property Name="varPersistentID:{84194344-4639-4979-9BFD-2AA3D640B413}" Type="Ref">/My Computer/Dependencies/Pad_Print_v3.lvlib/network_variables/Variable_graph</Property>
	<Property Name="varPersistentID:{84775BC3-78C3-405E-A5F5-6DACE7001897}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI8</Property>
	<Property Name="varPersistentID:{871BCB1F-CC6D-41C6-8130-D9AAFF097E3A}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO24</Property>
	<Property Name="varPersistentID:{89207F40-459A-460F-8F3E-D5B645548260}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO22</Property>
	<Property Name="varPersistentID:{89861FBE-030C-415F-AEFD-F85AC692E34B}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Outputs-Digital Outputs</Property>
	<Property Name="varPersistentID:{8AFE5CC1-BD98-405A-9144-8B49C8C63055}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO23</Property>
	<Property Name="varPersistentID:{8C997324-CDCE-4573-A892-C56FD2C6B044}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI28</Property>
	<Property Name="varPersistentID:{8E7BA2BB-A185-432F-84C8-8E228D765D74}" Type="Ref">/NI-cRIO9074-Padprint/Pad_Print_v3.lvlib/network_variables/time cliche</Property>
	<Property Name="varPersistentID:{9029A45E-6784-44B6-8C9B-C3FA3882448A}" Type="Ref">/NI-cRIO9074-Padprint/Pad_Print_v3.lvlib/network_variables/time sub</Property>
	<Property Name="varPersistentID:{930246E1-1212-496D-AA63-3020D62A4DE1}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO8</Property>
	<Property Name="varPersistentID:{945B59FD-B424-467F-A58B-B67EF12547A5}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI5</Property>
	<Property Name="varPersistentID:{947A2D34-8CFC-46BA-BADB-A79C095D0FEC}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Inputs-Position Actual Internal Value</Property>
	<Property Name="varPersistentID:{9625047C-BCA1-49EE-B958-B21C9E2A22EF}" Type="Ref">/My Computer/Dependencies/Pad_Print_v3.lvlib/network_variables/time sub</Property>
	<Property Name="varPersistentID:{996E90A2-6C79-45BF-A914-F42CE3F4FE59}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO5</Property>
	<Property Name="varPersistentID:{9B8146C5-AA02-4319-93B0-F40B9E7354F5}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Outputs-Torque Feed Forward</Property>
	<Property Name="varPersistentID:{9C05ADC3-ACEC-4251-9CEC-D3C24AF6D9C7}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Inputs-Second Position Feedback</Property>
	<Property Name="varPersistentID:{9EB16914-C49E-4154-AB3D-037130AAED15}" Type="Ref"></Property>
	<Property Name="varPersistentID:{A02BC11E-3AC0-42C0-8E71-DDE706EF2B53}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Outputs-Torque Feed Forward</Property>
	<Property Name="varPersistentID:{A682D046-0D7A-4818-8A68-6AC8A40D092E}" Type="Ref">/NI-cRIO9074-Padprint/Pad_Print_v3.lvlib/network_variables/Variable_graph</Property>
	<Property Name="varPersistentID:{A70CF2FF-526F-4987-9A4D-48A934C6E8BC}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO11</Property>
	<Property Name="varPersistentID:{A9AB6CEB-AC94-4C90-9390-401279625CF8}" Type="Ref">/My Computer/Dependencies/Pad_Print_v3.lvlib/network_variables/main prog cont switch</Property>
	<Property Name="varPersistentID:{AD1C8020-C162-4F29-9B36-583DAE652549}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI10</Property>
	<Property Name="varPersistentID:{B0DE548C-6849-45CA-874C-99FA6A445C15}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI15</Property>
	<Property Name="varPersistentID:{B0E692DD-52A7-4AD1-A1DE-90CA0E8E9D71}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO18</Property>
	<Property Name="varPersistentID:{B312180F-051E-4214-BB9C-C097EA7E80CC}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI16</Property>
	<Property Name="varPersistentID:{B5DF3C1A-8EEC-4FE2-BA83-33F95836388B}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI17</Property>
	<Property Name="varPersistentID:{B9216BC7-FF67-404F-A25D-0BAA0EF8F46F}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Inputs-Second Position Feedback</Property>
	<Property Name="varPersistentID:{BC0051F6-1B9A-49A2-BF8F-3123CD2E89AC}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO12</Property>
	<Property Name="varPersistentID:{C029BBCD-BAC6-4EF9-8955-C008FD1833C4}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI24</Property>
	<Property Name="varPersistentID:{C1267D93-E61A-421F-BA87-34813954E94E}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Inputs-Digital Inputs</Property>
	<Property Name="varPersistentID:{C13464B5-04CC-421E-8521-CDC1EC50314F}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO1</Property>
	<Property Name="varPersistentID:{C1A305BD-339F-49C9-AABE-268F8710BCFA}" Type="Ref"></Property>
	<Property Name="varPersistentID:{C37FFA7C-074A-47BB-A217-C3A1200973D2}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Inputs-Latch 1P</Property>
	<Property Name="varPersistentID:{C54AC25A-367B-4740-B73A-DB0968D0A475}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO2</Property>
	<Property Name="varPersistentID:{C96D75D9-A182-48A2-9BA5-B21F0DCEEB2F}" Type="Ref"></Property>
	<Property Name="varPersistentID:{CDCF9DF6-6157-45E0-8D98-98DBFF793EC6}" Type="Ref"></Property>
	<Property Name="varPersistentID:{CEEA1D40-1324-4BC3-9BDB-A28666654871}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO3</Property>
	<Property Name="varPersistentID:{D3D25E02-72B9-4063-8455-1EC7506FCF65}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI9</Property>
	<Property Name="varPersistentID:{D8D3776B-AAE6-4CA2-9FB3-A333F23B7284}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Inputs-Torque Actual Value</Property>
	<Property Name="varPersistentID:{DBD8FDD2-658D-44A4-A7E3-F0EE008906BE}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Inputs-Torque Actual Value</Property>
	<Property Name="varPersistentID:{DCD37137-4D15-49A2-B80E-130212F65004}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Outputs-Interpolation Data Record</Property>
	<Property Name="varPersistentID:{DCF4E380-D173-422B-9803-603A33645306}" Type="Ref">/My Computer/Dependencies/Pad_Print_v3.lvlib/network_variables/variables</Property>
	<Property Name="varPersistentID:{DD48D463-F5DE-4FBC-B2D7-3168F8B9CAB1}" Type="Ref"></Property>
	<Property Name="varPersistentID:{DEA4AF63-9F70-4756-813C-C2B27D6CF5F2}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI25</Property>
	<Property Name="varPersistentID:{E20C7C5B-E000-4871-A787-C0944B6C35A4}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI22</Property>
	<Property Name="varPersistentID:{E42BDD51-094E-4E72-A9F6-9563EFC5361B}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO27</Property>
	<Property Name="varPersistentID:{E47FD216-8442-4CBB-958C-AEA73D626F75}" Type="Ref"></Property>
	<Property Name="varPersistentID:{E74CB049-69E4-4BE3-8CBA-B5B32AE8C632}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO25</Property>
	<Property Name="varPersistentID:{E8263FB4-D282-4529-871C-E538076D3E4B}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI20</Property>
	<Property Name="varPersistentID:{E91E81B9-7CE3-47CB-B972-4528EF7BED37}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod4/AI3</Property>
	<Property Name="varPersistentID:{E9CE85B7-64D6-453A-83F8-FFE27D260538}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO9</Property>
	<Property Name="varPersistentID:{EA7BD0A0-36B6-44E2-9CFE-17DE66962711}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO13</Property>
	<Property Name="varPersistentID:{EBB72117-5E21-4B87-BD1E-FDCEE08F2CCA}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Outputs-Interpolation Data Record</Property>
	<Property Name="varPersistentID:{EFB09B9A-2A77-40C4-A1A1-13CB2AA2FF4E}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI1</Property>
	<Property Name="varPersistentID:{F06AD707-D9DA-4D4E-B29C-3F3F50FCAD49}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod4/AI2</Property>
	<Property Name="varPersistentID:{F5277F1A-BBC3-441C-8F1F-DC0D28000EFA}" Type="Ref">/My Computer/Dependencies/Pad_Print_v3.lvlib/network_variables/main prog indic</Property>
	<Property Name="varPersistentID:{F6286798-A419-4F2B-BB36-C3DCCFC3BF94}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Inputs-Latch Status</Property>
	<Property Name="varPersistentID:{F6564FB0-FDB7-4396-8C97-BB2044DC48A4}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device/Inputs-Latch Status</Property>
	<Property Name="varPersistentID:{F6649A61-5D5E-4026-BD37-D0492964D4C0}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO10</Property>
	<Property Name="varPersistentID:{F9DCDAE2-2919-4F9D-89A5-7F95994514CF}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO7</Property>
	<Property Name="varPersistentID:{F9F3D1F3-5E15-401C-A7D2-EBC40248E58A}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO19</Property>
	<Property Name="varPersistentID:{FA672D28-C449-45DF-B50F-F27707F2166A}" Type="Ref"></Property>
	<Property Name="varPersistentID:{FACA5656-15D2-4D78-9ADB-B91567FB1689}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Inputs-Velocity Actual Value</Property>
	<Property Name="varPersistentID:{FC84C7EF-51E2-4738-92BE-7DC2D7C0299A}" Type="Ref"></Property>
	<Property Name="varPersistentID:{FDD1E25A-C406-4E28-990A-43471F4F94B3}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod3/DO20</Property>
	<Property Name="varPersistentID:{FE208BA6-D6B1-4B56-8D3D-D343519BD90E}" Type="Ref">/NI-cRIO9074-Padprint/Chassis/Mod2/DI4</Property>
	<Property Name="varPersistentID:{FE9F68D4-198A-49E5-8519-9CD15D8F7F90}" Type="Ref">/NI-cRIO9074-Padprint/EtherCAT Master/Device2/Outputs-Control word</Property>
	<Property Name="varPersistentID:{FEE64304-8D9F-4D97-B5CC-8C8626B556AD}" Type="Ref">/NI-cRIO9074-Padprint/Pad_Print_v3.lvlib/network_variables/main prog indic</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="UI_IDD_Pad_Print_V_3.1.vi" Type="VI" URL="../../../extra/pad_print_test2/UI_IDD_Pad_Print_V_3.1.vi"/>
		<Item Name="Pad_Print_PC_v3.lvlib" Type="Library" URL="../Pad_Print_PC_v3.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Set Scan Engine Mode.vi" Type="VI" URL="/&lt;vilib&gt;/NIScanEngine/ScanEngine/Set Scan Engine Mode.vi"/>
				<Item Name="IOV engine refnum from target address.vi" Type="VI" URL="/&lt;vilib&gt;/NIScanEngine/ScanEngine/IOV engine refnum from target address.vi"/>
				<Item Name="Clear All Faults.vi" Type="VI" URL="/&lt;vilib&gt;/NIScanEngine/faults/Clear All Faults.vi"/>
				<Item Name="Get Scan Engine Mode.vi" Type="VI" URL="/&lt;vilib&gt;/NIScanEngine/ScanEngine/Get Scan Engine Mode.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
			</Item>
			<Item Name="Pad_Print_v3.lvlib" Type="Library" URL="../Pad_Print_v3.lvlib"/>
			<Item Name="Timer (SubVI).vi" Type="VI" URL="../../../extra/pad_print_test2/Timer (SubVI).vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="NI-cRIO9074-Padprint" Type="RT CompactRIO">
		<Property Name="alias.name" Type="Str">NI-cRIO9074-Padprint</Property>
		<Property Name="alias.value" Type="Str">130.83.101.242</Property>
		<Property Name="CCSymbols" Type="Str">OS,VxWorks;CPU,PowerPC;DeviceCode,729D;TARGET_TYPE,RT;</Property>
		<Property Name="crio.ControllerPID" Type="Str">729D</Property>
		<Property Name="crio.family" Type="Str">901x</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">2</Property>
		<Property Name="host.TargetOSID" Type="UInt">14</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str">1.0,0;</Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">4000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="UInt">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">false</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/c/ni-rt/startup</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str"></Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.viscripting.showScriptingOperationsInContextHelp" Type="Bool">false</Property>
		<Property Name="target.server.viscripting.showScriptingOperationsInEditor" Type="Bool">false</Property>
		<Property Name="target.WebServer.Config" Type="Str"># Web server configuration file.
# Generated by LabVIEW 13.0f2
# 04.08.2023 11:56:49

#
# Global Directives
#
NI.AddLVRouteVars
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
LimitWorkers 10
LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule
Listen 8000

#
# Directives that apply to the default server
#
NI.ServerName LabVIEW
DocumentRoot "$LVSERVER_DOCROOT"
InactivityTimeout 60
SetConnector netConnector
AddHandler LVAuth
AddHandler LVRFP
AddHandler fileHandler ""
AddOutputFilter chunkFilter
DirectoryIndex index.htm
</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="Axis 1" Type="Motion Axis">
			<Property Name="axis.class:0" Type="Int">12714031</Property>
			<Property Name="axis.mapping:0" Type="Str">EtherCAT Master\Device (Address 0, AKD)</Property>
			<Property Name="axis.SMVersion" Type="Int">201310</Property>
			<Property Name="axisSetup.activeOnModeTransition:0" Type="Bool">true</Property>
			<Property Name="axisSetup.deployDriveParameters:0" Type="Bool">true</Property>
			<Property Name="controlLoop.positionLoop.gains.proportionalGain:0" Type="Dbl">94,24777960500001</Property>
			<Property Name="controlLoop.velocityLoop.gains.proportionalGain:0" Type="Dbl">0,2389380328014085</Property>
			<Property Name="deviceID:0" Type="Int">4279108</Property>
			<Property Name="encoder.countsPerUnit:0" Type="Dbl">104857,6</Property>
			<Property Name="encoder.units:0" Type="Str">mm</Property>
			<Property Name="homeSwitch.enable:0" Type="Bool">true</Property>
			<Property Name="interactivePanel.acceleration:0" Type="Dbl">1000</Property>
			<Property Name="interactivePanel.deceleration:0" Type="Dbl">1000</Property>
			<Property Name="interactivePanel.targetPosition:0" Type="Dbl">10</Property>
			<Property Name="interactivePanel.velocity:0" Type="Dbl">40</Property>
			<Property Name="resource.type:0" Type="Int">10551297</Property>
			<Property Name="resourceID:0" Type="Str">{BEB31281-8A39-4E98-BFBF-1BD06FD9AAAD}</Property>
			<Property Name="softmotionID:0" Type="Str">{C89C22D6-09D8-451c-B54B-BE98A931F881}</Property>
			<Property Name="trajectory.velocityFilter.distance:0" Type="Dbl">1</Property>
			<Property Name="trajectory.velocityThreshold:0" Type="Dbl">650</Property>
			<Property Name="tuningPanel.acceleration:0" Type="Dbl">1000</Property>
			<Property Name="tuningPanel.deceleration:0" Type="Dbl">1000</Property>
			<Property Name="tuningPanel.moveDistance:0" Type="Dbl">-20</Property>
			<Property Name="tuningPanel.positionErrorLimit:0" Type="Dbl">115</Property>
			<Property Name="tuningPanel.sliderTuning.loadInertia:0" Type="Dbl">1</Property>
			<Property Name="tuningPanel.stepLength:0" Type="Dbl">-10</Property>
			<Property Name="tuningPanel.stepSamples:0" Type="Dbl">7</Property>
			<Property Name="tuningPanel.velocity:0" Type="Dbl">100</Property>
			<Property Name="vendorID:0" Type="Int">106</Property>
		</Item>
		<Item Name="Axis 2" Type="Motion Axis">
			<Property Name="axis.class:0" Type="Int">12714031</Property>
			<Property Name="axis.ecatSlaveHandle:0" Type="Int">1</Property>
			<Property Name="axis.mapping:0" Type="Str">EtherCAT Master\Device2 (Address 1, AKD)</Property>
			<Property Name="axis.SMVersion" Type="Int">201310</Property>
			<Property Name="axisSetup.activeOnModeTransition:0" Type="Bool">true</Property>
			<Property Name="axisSetup.deployDriveParameters:0" Type="Bool">true</Property>
			<Property Name="deviceID:0" Type="Int">4279108</Property>
			<Property Name="encoder.countsPerUnit:0" Type="Dbl">510007,782101</Property>
			<Property Name="encoder.units:0" Type="Str">mm</Property>
			<Property Name="homeSwitch.enable:0" Type="Bool">true</Property>
			<Property Name="interactivePanel.acceleration:0" Type="Dbl">100</Property>
			<Property Name="interactivePanel.deceleration:0" Type="Dbl">100</Property>
			<Property Name="interactivePanel.targetPosition:0" Type="Dbl">-1</Property>
			<Property Name="interactivePanel.velocity:0" Type="Dbl">40</Property>
			<Property Name="resource.type:0" Type="Int">10551297</Property>
			<Property Name="resourceID:0" Type="Str">{CFB3DBEA-075D-4E40-9076-DD32210E6551}</Property>
			<Property Name="softmotionID:0" Type="Str">{C89C22D6-09D8-451c-B54B-BE98A931F881}</Property>
			<Property Name="trajectory.velocityFilter.distance:0" Type="Dbl">1</Property>
			<Property Name="trajectory.velocityThreshold:0" Type="Dbl">250</Property>
			<Property Name="vendorID:0" Type="Int">106</Property>
		</Item>
		<Item Name="Chassis" Type="cRIO Chassis">
			<Property Name="crio.ProgrammingMode" Type="Str">express</Property>
			<Property Name="crio.ResourceID" Type="Str">RIO0</Property>
			<Property Name="crio.Type" Type="Str">cRIO-9074</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Mod2" Type="RIO C Series Module">
				<Property Name="crio.Calibration" Type="Str">1</Property>
				<Property Name="crio.Location" Type="Str">Slot 2</Property>
				<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
				<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
				<Property Name="crio.Type" Type="Str">NI 9425</Property>
				<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
				<Item Name="DI0" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI0</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI1" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI1</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI2" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI2</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI3" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI3</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI4" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI4</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI5" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI5</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI6" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI6</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI7" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI7</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI8" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI8</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI9" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI9</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI10" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI10</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI11" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI11</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI12" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI12</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI13" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI13</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI14" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI14</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI15" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI15</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI16" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">16</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI16</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI17" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">17</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI17</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI18" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">18</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI18</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI19" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">19</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI19</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI20" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">20</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI20</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI21" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">21</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI21</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI22" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">22</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI22</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI23" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">23</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI23</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI24" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">24</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI24</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI25" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">25</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI25</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI26" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">26</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI26</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI27" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">27</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI27</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI28" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">28</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI28</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI29" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">29</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI29</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI30" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">30</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI30</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DI31" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">31</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DI31</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
			</Item>
			<Item Name="Mod3" Type="RIO C Series Module">
				<Property Name="crio.Calibration" Type="Str">1</Property>
				<Property Name="crio.Location" Type="Str">Slot 3</Property>
				<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
				<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
				<Property Name="crio.Type" Type="Str">NI 9476</Property>
				<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
				<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
				<Item Name="DO0" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO0</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO1" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO1</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO2" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO2</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO3" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO3</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO4" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO4</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO5" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO5</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO6" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO6</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO7" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO7</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO8" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO8</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO9" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO9</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO10" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO10</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO11" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO11</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO12" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO12</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO13" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO13</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO14" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO14</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO15" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO15</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO16" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">16</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO16</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO17" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">17</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO17</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO18" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">18</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO18</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO19" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">19</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO19</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO20" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">20</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO20</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO21" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">21</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO21</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO22" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">22</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO22</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO23" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">23</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO23</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO24" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">24</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO24</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO25" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">25</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO25</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO26" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">26</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO26</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO27" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">27</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO27</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO28" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">28</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO28</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO29" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">29</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO29</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO30" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">30</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO30</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="DO31" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">31</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO31</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"-!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
			</Item>
			<Item Name="Mod4" Type="RIO C Series Module">
				<Property Name="crio.Calibration" Type="Str">1</Property>
				<Property Name="crio.Location" Type="Str">Slot 4</Property>
				<Property Name="crio.RequiresValidation" Type="Bool">true</Property>
				<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
				<Property Name="crio.Type" Type="Str">NI 9237</Property>
				<Property Name="cRIOModule.ClockSource" Type="Str">0</Property>
				<Property Name="cRIOModule.DataRate" Type="Str">1</Property>
				<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
				<Property Name="cRIOModule.ExcitationVoltage" Type="Str">1</Property>
				<Property Name="cRIOModule.ExternalClockSource" Type="Str"></Property>
				<Property Name="cRIOModule.ExtTimeBaseType" Type="Str"></Property>
				<Property Name="cRIOModule.HalfBridgeEnable" Type="Str">0</Property>
				<Property Name="cRIOModule.InputConfiguration" Type="Str">0</Property>
				<Property Name="cRIOModule.SourceModule" Type="Str">false</Property>
				<Property Name="cRIOModule.SubPanVisitedVersion" Type="Str">0</Property>
				<Property Name="cRIOModule.TEDSSupport" Type="Str">true</Property>
				<Item Name="AI0" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AI0</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"-!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="AI1" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AI1</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"-!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="AI2" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AI2</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"-!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="AI3" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AI3</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"-!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="EtherCAT Master" Type="ECATMaster">
			<Property Name="ECAT.Adapter.Index" Type="UInt">0</Property>
			<Property Name="ECAT.Master.Comments" Type="Str"></Property>
			<Property Name="ECAT.Master.CyclicPercentage" Type="UInt">40</Property>
			<Property Name="ECAT.Master.LRWEnabled" Type="Bool">true</Property>
			<Property Name="ECAT.Master.UserShift" Type="Int">0</Property>
			<Property Name="ECAT.Name" Type="Str">EtherCAT Master</Property>
			<Item Name="Device" Type="ECATEnetSlave">
				<Property Name="ECAT.Description" Type="Str">AKD EtherCAT Drive (CoE)</Property>
				<Property Name="ECAT.Name" Type="Str">Device</Property>
				<Property Name="ECAT.Slave.CoESupported" Type="Bool">true</Property>
				<Property Name="ECAT.Slave.DCEnabled" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.DCSupported" Type="Bool">true</Property>
				<Property Name="ECAT.Slave.E2PROMSize" Type="UInt">2048</Property>
				<Property Name="ECAT.Slave.EoESupported" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.FmmuCount" Type="UInt">2</Property>
				<Property Name="ECAT.Slave.FoESupported" Type="Bool">true</Property>
				<Property Name="ECAT.Slave.InitCmd.Comment0" Type="Str">Opmode</Property>
				<Property Name="ECAT.Slave.InitCmd.Comment1" Type="Str">Cycle time</Property>
				<Property Name="ECAT.Slave.InitCmd.Comment2" Type="Str">Cycle exp</Property>
				<Property Name="ECAT.Slave.InitCmd.Data0" Type="Str">07</Property>
				<Property Name="ECAT.Slave.InitCmd.Data1" Type="Str">02</Property>
				<Property Name="ECAT.Slave.InitCmd.Data2" Type="Str">fd</Property>
				<Property Name="ECAT.Slave.InitCmd.DependOnSlot0" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.InitCmd.DependOnSlot1" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.InitCmd.DependOnSlot2" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.InitCmd.DestinationState0" Type="UInt">4</Property>
				<Property Name="ECAT.Slave.InitCmd.DestinationState1" Type="UInt">4</Property>
				<Property Name="ECAT.Slave.InitCmd.DestinationState2" Type="UInt">4</Property>
				<Property Name="ECAT.Slave.InitCmd.Num" Type="UInt">3</Property>
				<Property Name="ECAT.Slave.InitCmd.ObjectIndex0" Type="UInt">24672</Property>
				<Property Name="ECAT.Slave.InitCmd.ObjectIndex1" Type="UInt">24770</Property>
				<Property Name="ECAT.Slave.InitCmd.ObjectIndex2" Type="UInt">24770</Property>
				<Property Name="ECAT.Slave.InitCmd.ObjectSubIndex0" Type="UInt">0</Property>
				<Property Name="ECAT.Slave.InitCmd.ObjectSubIndex1" Type="UInt">1</Property>
				<Property Name="ECAT.Slave.InitCmd.ObjectSubIndex2" Type="UInt">2</Property>
				<Property Name="ECAT.Slave.InitCmd.SourceState0" Type="UInt">2</Property>
				<Property Name="ECAT.Slave.InitCmd.SourceState1" Type="UInt">2</Property>
				<Property Name="ECAT.Slave.InitCmd.SourceState2" Type="UInt">2</Property>
				<Property Name="ECAT.Slave.InputCorrection" Type="Int">0</Property>
				<Property Name="ECAT.Slave.InputSyncMode" Type="UInt">0</Property>
				<Property Name="ECAT.Slave.IsComplex" Type="Bool">true</Property>
				<Property Name="ECAT.Slave.IsWatchdogTimeoutByScan" Type="Bool">true</Property>
				<Property Name="ECAT.Slave.MB.PollRateByScan" Type="UInt">1</Property>
				<Property Name="ECAT.Slave.NodeAddress" Type="UInt">4096</Property>
				<Property Name="ECAT.Slave.OutputCorrection" Type="Int">0</Property>
				<Property Name="ECAT.Slave.OutputSyncMode" Type="UInt">0</Property>
				<Property Name="ECAT.Slave.PDIWatchdogTimeoutByInc" Type="UInt">1000</Property>
				<Property Name="ECAT.Slave.PDIWDEnabled" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.PDIWDTime" Type="UInt">10</Property>
				<Property Name="ECAT.Slave.PositionAddress" Type="UInt">0</Property>
				<Property Name="ECAT.Slave.ProductCode" Type="UInt">4279108</Property>
				<Property Name="ECAT.Slave.RevisionNo" Type="UInt">2</Property>
				<Property Name="ECAT.Slave.SelectedPDOIndex" Type="Str">6944:5923:</Property>
				<Property Name="ECAT.Slave.SerialNo" Type="UInt">2760769601</Property>
				<Property Name="ECAT.Slave.SMCount" Type="UInt">4</Property>
				<Property Name="ECAT.Slave.SMWatchdogTimeoutByInc" Type="UInt">1000</Property>
				<Property Name="ECAT.Slave.SMWDEnabled" Type="Bool">true</Property>
				<Property Name="ECAT.Slave.SMWDTime" Type="UInt">10</Property>
				<Property Name="ECAT.Slave.Sync0CycleByScan" Type="UInt">1</Property>
				<Property Name="ECAT.Slave.Sync0Enabled" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.Sync0Shift" Type="UInt">0</Property>
				<Property Name="ECAT.Slave.Sync1CycleBySync0Cycle" Type="UInt">1</Property>
				<Property Name="ECAT.Slave.Sync1Enabled" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.Sync1Shift" Type="UInt">0</Property>
				<Property Name="ECAT.Slave.Type" Type="Str">AKD</Property>
				<Property Name="ECAT.Slave.VendorID" Type="UInt">106</Property>
				<Property Name="ECAT.Slave.VendorName" Type="Str">Kollmorgen</Property>
				<Property Name="ECAT.Slave.WatchdogDivider" Type="UInt">2498</Property>
				<Property Name="NI.SortType" Type="Int">1</Property>
				<Item Name="Inputs-Digital Inputs" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">3</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Digital Inputs</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Following Error Actual Value" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">4</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Following Error Actual Value</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Latch 1P" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">5</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Latch 1P</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Latch Status" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">8</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Latch Status</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!"A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Position Actual Internal Value" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">0</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Position Actual Internal Value</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Second Position Feedback" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">1</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Second Position Feedback</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Status word" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">6</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Status word</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!"A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Torque Actual Value" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">7</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Torque Actual Value</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!!A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Velocity Actual Value" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">2</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Velocity Actual Value</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Outputs-ClearDigInputChangedBit" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">20</Property>
					<Property Name="ECAT.Index" Type="UInt">5</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">false</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Outputs-ClearDigInputChangedBit</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!"A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Outputs-Control word" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">20</Property>
					<Property Name="ECAT.Index" Type="UInt">0</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">false</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Outputs-Control word</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!"A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Outputs-Digital Outputs" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">20</Property>
					<Property Name="ECAT.Index" Type="UInt">4</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">false</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Outputs-Digital Outputs</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Outputs-Interpolation Data Record" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">20</Property>
					<Property Name="ECAT.Index" Type="UInt">1</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">false</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Outputs-Interpolation Data Record</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Outputs-Latch Control Word" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">20</Property>
					<Property Name="ECAT.Index" Type="UInt">2</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">false</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Outputs-Latch Control Word</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!"A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Outputs-Torque Feed Forward" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">20</Property>
					<Property Name="ECAT.Index" Type="UInt">3</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">false</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Outputs-Torque Feed Forward</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!!A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-AIN.VALUE" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">9</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-AIN.VALUE</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!!A!!!1!!!!!!!!!!</Property>
				</Item>
			</Item>
			<Item Name="Device2" Type="ECATEnetSlave">
				<Property Name="ECAT.Description" Type="Str">AKD EtherCAT Drive (CoE)</Property>
				<Property Name="ECAT.Name" Type="Str">Device2</Property>
				<Property Name="ECAT.Slave.CoESupported" Type="Bool">true</Property>
				<Property Name="ECAT.Slave.DCEnabled" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.DCSupported" Type="Bool">true</Property>
				<Property Name="ECAT.Slave.E2PROMSize" Type="UInt">2048</Property>
				<Property Name="ECAT.Slave.EoESupported" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.FmmuCount" Type="UInt">2</Property>
				<Property Name="ECAT.Slave.FoESupported" Type="Bool">true</Property>
				<Property Name="ECAT.Slave.InitCmd.Comment0" Type="Str">Opmode</Property>
				<Property Name="ECAT.Slave.InitCmd.Comment1" Type="Str">Cycle time</Property>
				<Property Name="ECAT.Slave.InitCmd.Comment2" Type="Str">Cycle exp</Property>
				<Property Name="ECAT.Slave.InitCmd.Data0" Type="Str">07</Property>
				<Property Name="ECAT.Slave.InitCmd.Data1" Type="Str">02</Property>
				<Property Name="ECAT.Slave.InitCmd.Data2" Type="Str">fd</Property>
				<Property Name="ECAT.Slave.InitCmd.DependOnSlot0" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.InitCmd.DependOnSlot1" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.InitCmd.DependOnSlot2" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.InitCmd.DestinationState0" Type="UInt">4</Property>
				<Property Name="ECAT.Slave.InitCmd.DestinationState1" Type="UInt">4</Property>
				<Property Name="ECAT.Slave.InitCmd.DestinationState2" Type="UInt">4</Property>
				<Property Name="ECAT.Slave.InitCmd.Num" Type="UInt">3</Property>
				<Property Name="ECAT.Slave.InitCmd.ObjectIndex0" Type="UInt">24672</Property>
				<Property Name="ECAT.Slave.InitCmd.ObjectIndex1" Type="UInt">24770</Property>
				<Property Name="ECAT.Slave.InitCmd.ObjectIndex2" Type="UInt">24770</Property>
				<Property Name="ECAT.Slave.InitCmd.ObjectSubIndex0" Type="UInt">0</Property>
				<Property Name="ECAT.Slave.InitCmd.ObjectSubIndex1" Type="UInt">1</Property>
				<Property Name="ECAT.Slave.InitCmd.ObjectSubIndex2" Type="UInt">2</Property>
				<Property Name="ECAT.Slave.InitCmd.SourceState0" Type="UInt">2</Property>
				<Property Name="ECAT.Slave.InitCmd.SourceState1" Type="UInt">2</Property>
				<Property Name="ECAT.Slave.InitCmd.SourceState2" Type="UInt">2</Property>
				<Property Name="ECAT.Slave.InputCorrection" Type="Int">0</Property>
				<Property Name="ECAT.Slave.InputSyncMode" Type="UInt">0</Property>
				<Property Name="ECAT.Slave.IsComplex" Type="Bool">true</Property>
				<Property Name="ECAT.Slave.IsWatchdogTimeoutByScan" Type="Bool">true</Property>
				<Property Name="ECAT.Slave.MB.PollRateByScan" Type="UInt">1</Property>
				<Property Name="ECAT.Slave.NodeAddress" Type="UInt">4097</Property>
				<Property Name="ECAT.Slave.OutputCorrection" Type="Int">0</Property>
				<Property Name="ECAT.Slave.OutputSyncMode" Type="UInt">0</Property>
				<Property Name="ECAT.Slave.PDIWatchdogTimeoutByInc" Type="UInt">1000</Property>
				<Property Name="ECAT.Slave.PDIWDEnabled" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.PDIWDTime" Type="UInt">10</Property>
				<Property Name="ECAT.Slave.PositionAddress" Type="UInt">1</Property>
				<Property Name="ECAT.Slave.ProductCode" Type="UInt">4279108</Property>
				<Property Name="ECAT.Slave.RevisionNo" Type="UInt">2</Property>
				<Property Name="ECAT.Slave.SelectedPDOIndex" Type="Str">6944:5923:</Property>
				<Property Name="ECAT.Slave.SerialNo" Type="UInt">2760769602</Property>
				<Property Name="ECAT.Slave.SMCount" Type="UInt">4</Property>
				<Property Name="ECAT.Slave.SMWatchdogTimeoutByInc" Type="UInt">1000</Property>
				<Property Name="ECAT.Slave.SMWDEnabled" Type="Bool">true</Property>
				<Property Name="ECAT.Slave.SMWDTime" Type="UInt">10</Property>
				<Property Name="ECAT.Slave.Sync0CycleByScan" Type="UInt">1</Property>
				<Property Name="ECAT.Slave.Sync0Enabled" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.Sync0Shift" Type="UInt">0</Property>
				<Property Name="ECAT.Slave.Sync1CycleBySync0Cycle" Type="UInt">1</Property>
				<Property Name="ECAT.Slave.Sync1Enabled" Type="Bool">false</Property>
				<Property Name="ECAT.Slave.Sync1Shift" Type="UInt">0</Property>
				<Property Name="ECAT.Slave.Type" Type="Str">AKD</Property>
				<Property Name="ECAT.Slave.VendorID" Type="UInt">106</Property>
				<Property Name="ECAT.Slave.VendorName" Type="Str">Kollmorgen</Property>
				<Property Name="ECAT.Slave.WatchdogDivider" Type="UInt">2498</Property>
				<Property Name="NI.SortType" Type="Int">1</Property>
				<Item Name="Inputs-Digital Inputs" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">3</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Digital Inputs</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Following Error Actual Value" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">4</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Following Error Actual Value</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Latch 1P" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">5</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Latch 1P</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Latch Status" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">8</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Latch Status</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!"A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Position Actual Internal Value" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">0</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Position Actual Internal Value</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Second Position Feedback" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">1</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Second Position Feedback</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Status word" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">6</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Status word</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!"A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Torque Actual Value" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">7</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Torque Actual Value</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!!A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-Velocity Actual Value" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">2</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-Velocity Actual Value</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Outputs-ClearDigInputChangedBit" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">20</Property>
					<Property Name="ECAT.Index" Type="UInt">5</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">false</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Outputs-ClearDigInputChangedBit</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!"A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Outputs-Control word" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">20</Property>
					<Property Name="ECAT.Index" Type="UInt">0</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">false</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Outputs-Control word</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!"A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Outputs-Digital Outputs" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">20</Property>
					<Property Name="ECAT.Index" Type="UInt">4</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">false</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Outputs-Digital Outputs</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Outputs-Interpolation Data Record" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">20</Property>
					<Property Name="ECAT.Index" Type="UInt">1</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">false</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Outputs-Interpolation Data Record</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">'1!!!"-!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="Outputs-Latch Control Word" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">20</Property>
					<Property Name="ECAT.Index" Type="UInt">2</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">false</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Outputs-Latch Control Word</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!"A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Outputs-Torque Feed Forward" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">20</Property>
					<Property Name="ECAT.Index" Type="UInt">3</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">false</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Outputs-Torque Feed Forward</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!!A!!!1!!!!!!!!!!</Property>
				</Item>
				<Item Name="Inputs-AIN.VALUE" Type="Variable">
					<Property Name="_pdoIndex" Type="UInt">5</Property>
					<Property Name="ECAT.Index" Type="UInt">9</Property>
					<Property Name="ECAT.PDO.IsTxPDO" Type="Bool">true</Property>
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
					<Property Name="Industrial:IODirection" Type="Str">Input</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">Inputs-AIN.VALUE</Property>
					<Property Name="Network:BuffSize" Type="Str">50</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">True</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;Q!!!"-!A!!!!!!"!!5!!A!!!1!!!!!!!!!!</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Main_IDD_Pad_Print_V_3.vi" Type="VI" URL="../Main_IDD_Pad_Print_V_3.vi"/>
		<Item Name="Main_IDD_Pad_Print_V_3_1.vi" Type="VI" URL="../Main_IDD_Pad_Print_V_3_1.vi"/>
		<Item Name="Pad_Print_v3.lvlib" Type="Library" URL="../Pad_Print_v3.lvlib"/>
		<Item Name="test.lvlib" Type="Library" URL="../test.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Get Scan Engine Mode.vi" Type="VI" URL="/&lt;vilib&gt;/NIScanEngine/ScanEngine/Get Scan Engine Mode.vi"/>
				<Item Name="IOV engine refnum from target address.vi" Type="VI" URL="/&lt;vilib&gt;/NIScanEngine/ScanEngine/IOV engine refnum from target address.vi"/>
				<Item Name="Clear All Faults.vi" Type="VI" URL="/&lt;vilib&gt;/NIScanEngine/faults/Clear All Faults.vi"/>
				<Item Name="Set Scan Engine Mode.vi" Type="VI" URL="/&lt;vilib&gt;/NIScanEngine/ScanEngine/Set Scan Engine Mode.vi"/>
				<Item Name="ni_emb.dll" Type="Document" URL="/&lt;vilib&gt;/ni_emb.dll"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="nisyscfg.lvlib" Type="Library" URL="/&lt;vilib&gt;/nisyscfg/nisyscfg.lvlib"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="nimc.destroy.vi" Type="VI" URL="/&lt;vilib&gt;/Motion/PropertyNodes/nimc.destroy.vi"/>
				<Item Name="nimc.create.vi" Type="VI" URL="/&lt;vilib&gt;/Motion/PropertyNodes/nimc.create.vi"/>
				<Item Name="NI_Real-Time Target Support.lvlib" Type="Library" URL="/&lt;vilib&gt;/NI_Real-Time Target Support.lvlib"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
			</Item>
			<Item Name="Timer (SubVI).vi" Type="VI" URL="../../../extra/pad_print_test2/Timer (SubVI).vi"/>
			<Item Name="Pad_Print_PC_v3.lvlib" Type="Library" URL="../Pad_Print_PC_v3.lvlib"/>
			<Item Name="Global.vi" Type="VI" URL="../../../extra/pad_print_test2/C/Program Files (x86)/National Instruments/LabVIEW 2013/examples/Motion/PropertyNodes/Axis Line Move State Machine/Global.vi"/>
			<Item Name="actor_state_machine.vi" Type="VI" URL="../actor_state_machine.vi"/>
			<Item Name="nimclv.dll" Type="Document" URL="nimclv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>

﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="FPGA_Bitfiles" Type="Folder">
		<Item Name="padprintv31_FPGATarget_FPGAloadCellexam_StK4biHTQmA_copy.lvbitx" Type="Document" URL="../FPGA Bitfiles/padprintv31_FPGATarget_FPGAloadCellexam_StK4biHTQmA_copy.lvbitx"/>
	</Item>
	<Item Name="network_variables" Type="Folder">
		<Item Name="main prog cont switch" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">2</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typedefName1" Type="Str">Pad_Print_v3_1.lvlib:high_level_control.ctl</Property>
			<Property Name="typedefName2" Type="Str">Pad_Print_v3_1.lvlib:main prog cont switch.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../high_level_control.ctl</Property>
			<Property Name="typedefPath2" Type="PathRel">../main prog cont switch.ctl</Property>
			<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(2T1%!!#!!A!!!!!!.!!J!)16)4%5A1A!/1#%)2%]W)%RV:H1!!""!)1N%4T%V)&amp;:B9X6V&lt;1!+1#%%6E636!!!$%!B"T*@5X2B=H1!%%!B#T&amp;@2GFO:#"I&lt;WVF!""!)1N4&gt;'^Q)%*V&gt;(2P&lt;A!/1#%)3'^S)'^O&lt;(E!!"2!)1Z$&lt;#YA5(*F=SYA1X2S&lt;!!!&amp;%!B$F"S,C"1=G6T,C"$&gt;(*M!!#0!0%!!!!!!!!!!B21972@5(*J&lt;H2@&gt;D.@-3ZM&gt;GRJ9B:I;7&gt;I8WRF&gt;G6M8W.P&lt;H2S&lt;WQO9X2M!&amp;N!&amp;A!&amp;%X&gt;B;81A:G^S)(6T:8)A;7ZQ&gt;81*:GFO:#"I&lt;WVF"8.U98*U$G.M;7.I:3"T:8*W;7.F$8*F=W^M&gt;G5A:8*S&lt;X)!!!VV=W6S)'.P97VN97ZE!"N!!Q!5&lt;H6N9G6S)'^G)'FU:8*B&gt;'FP&lt;H-!!']!]1!!!!!!!!!#&amp;&amp;"B:&amp;^1=GFO&gt;&amp;^W-V]R,GRW&lt;'FC'7VB;7YA=(*P:S"D&lt;WZU)(.X;82D;#ZD&gt;'Q!/%"1!!Q!!!!"!!)!!Q!%!!5!"A!(!!A!#1!+!!M6&lt;7&amp;J&lt;C"Q=G^H)'.P&lt;H1A=X&gt;J&gt;'.I!!%!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="main prog indic" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">2</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typedefName1" Type="Str">Pad_Print_v3_1.lvlib:case_control.ctl</Property>
			<Property Name="typedefName2" Type="Str">Pad_Print_v3_1.lvlib:main prog indicator.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../case_control.ctl</Property>
			<Property Name="typedefPath2" Type="PathRel">../main prog indicator.ctl</Property>
			<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!AD(QA!!#!!A!!!!!!-!!R!)1:797.V&lt;75!!"&gt;!#A!127RB=(.F:#!+6'FN:3"$&lt;!!!&amp;U!+!"&amp;&amp;&lt;'&amp;Q=W6E)!J5;7VF)&amp;.V9A*,!0%!!!!!!!!!!B21972@5(*J&lt;H2@&gt;D.@-3ZM&gt;GRJ9B"D98.F8W.P&lt;H2S&lt;WQO9X2M!BV!&amp;A!6(EFO;81A-4IA6W&amp;J&gt;#"';7ZE)%BP&lt;75A9W^N&lt;7&amp;O:"R*&lt;GFU)$)[)%:J&lt;G1A3'^N:3"1&lt;X.J&gt;'FP&lt;C":(%FO;81A-TIA2GFO:#")&lt;WVF)&amp;"P=WFU;7^O)&amp;A(6W&amp;J&gt;#"03RB13$%[)%.M;7.I:3"N&lt;X:F)':P=H&gt;B=G185%AS/C"1971A&lt;7^W:3"U&lt;S"$&lt;'FD;'5?5%AS9DIA1WRJ9WBF)("S:7&amp;T=X6S:3"D&lt;WZU=G^M&amp;V")-TIA5'&amp;E)(&gt;B;81A&lt;WYA1WRJ9WBF(F").$IA5'&amp;E)'VP&gt;G5A98&gt;B?3"G=G^N)%.M;7.I:3&amp;13$5[)%.M;7.I:3"N&lt;X:F)(2P)%BP&lt;75A5'^T;82J&lt;WY;5%AW/C"1971A&lt;7^W:3"U&lt;S"4&gt;7*T&gt;(*B&gt;'5B5%AW9DIA5X6C=X2S982F)("S:7&amp;T=X6S:3"D&lt;WZU=G^M'F").TIA5'&amp;E)(&gt;B;81A&lt;WYA5X6C=X2S982F(F")/$IA5'&amp;E)'VP&gt;G5A&gt;']A3'^N:3"1&lt;X.J&gt;'FP&lt;B.13&amp;A[)'ZF?(1A;82F=G&amp;U;7^O'5-R/C"$&lt;'FD;'5A5W6S&gt;GFD:3!N)'FO;81&lt;1T)[)%.M;7.I:3"4:8*W;7.F)#UA97.U;8:F'5-T/C"$&lt;'FD;'5A5W6S&gt;GFD:3!N)'6Y;81)3%QA28*S&lt;X))4%QA28*S&lt;X).5G6T&lt;WRW:3"&amp;=H*P=A!!#E6S=G^S)'.B=W5!!EU!]1!!!!!!!!!#&amp;&amp;"B:&amp;^1=GFO&gt;&amp;^W-V]R,GRW&lt;'FC%'.B=W6@9W^O&gt;(*P&lt;#ZD&gt;'Q#(U!7!"5?37ZJ&gt;#!R/C"897FU)%:J&lt;G1A3'^N:3"D&lt;WVN97ZE(%FO;81A-DIA2GFO:#")&lt;WVF)&amp;"P=WFU;7^O)&amp;E=37ZJ&gt;#!T/C"';7ZE)%BP&lt;75A5'^T;82J&lt;WYA7!&gt;897FU)%^,'&amp;")-4IA1WRJ9WBF)'VP&gt;G5A:G^S&gt;W&amp;S:"&gt;13$)[)&amp;"B:#"N&lt;X:F)(2P)%.M;7.I:2Z13$*C/C"$&lt;'FD;'5A=(*F98.T&gt;8*F)'.P&lt;H2S&lt;WQ85%AT/C"1971A&gt;W&amp;J&gt;#"P&lt;C"$&lt;'FD;'5?5%AU/C"1971A&lt;7^W:3"B&gt;W&amp;Z)':S&lt;WUA1WRJ9WBF)6").4IA1WRJ9WBF)'VP&gt;G5A&gt;']A3'^N:3"1&lt;X.J&gt;'FP&lt;BJ13$9[)&amp;"B:#"N&lt;X:F)(2P)&amp;.V9H.U=G&amp;U:3&amp;13$:C/C"4&gt;7*T&gt;(*B&gt;'5A=(*F98.T&gt;8*F)'.P&lt;H2S&lt;WQ;5%AX/C"1971A&gt;W&amp;J&gt;#"P&lt;C"4&gt;7*T&gt;(*B&gt;'5?5%AY/C"1971A&lt;7^W:3"U&lt;S")&lt;WVF)&amp;"P=WFU;7^O%V")7$IA&lt;G6Y&gt;#"J&gt;'6S982J&lt;WY:1T%[)%.M;7.I:3"4:8*W;7.F)#UA;7ZJ&gt;"N$-DIA1WRJ9WBF)&amp;.F=H:J9W5A,3"B9X2J&gt;G5:1T-[)%.M;7.I:3"4:8*W;7.F)#UA:8BJ&gt;!B)4#"&amp;=H*P=AB-4#"&amp;=H*P=AV3:8.P&lt;(:F)%6S=G^S!!!.9X6S=G6O&gt;#"T&gt;'&amp;U:1!51#%/=(*J&lt;H1A:GFO;8.I:71!!E]!]1!!!!!!!!!#&amp;&amp;"B:&amp;^1=GFO&gt;&amp;^W-V]R,GRW&lt;'FC%'.B=W6@9W^O&gt;(*P&lt;#ZD&gt;'Q#)5!7!"5?37ZJ&gt;#!R/C"897FU)%:J&lt;G1A3'^N:3"D&lt;WVN97ZE(%FO;81A-DIA2GFO:#")&lt;WVF)&amp;"P=WFU;7^O)&amp;E=37ZJ&gt;#!T/C"';7ZE)%BP&lt;75A5'^T;82J&lt;WYA7!&gt;897FU)%^,'&amp;")-4IA1WRJ9WBF)'VP&gt;G5A:G^S&gt;W&amp;S:"&gt;13$)[)&amp;"B:#"N&lt;X:F)(2P)%.M;7.I:2Z13$*C/C"$&lt;'FD;'5A=(*F98.T&gt;8*F)'.P&lt;H2S&lt;WQ85%AT/C"1971A&gt;W&amp;J&gt;#"P&lt;C"$&lt;'FD;'5?5%AU/C"1971A&lt;7^W:3"B&gt;W&amp;Z)':S&lt;WUA1WRJ9WBF)6").4IA1WRJ9WBF)'VP&gt;G5A&gt;']A3'^N:3"1&lt;X.J&gt;'FP&lt;BJ13$9[)&amp;"B:#"N&lt;X:F)(2P)&amp;.V9H.U=G&amp;U:3&amp;13$:C/C"4&gt;7*T&gt;(*B&gt;'5A=(*F98.T&gt;8*F)'.P&lt;H2S&lt;WQ;5%AX/C"1971A&gt;W&amp;J&gt;#"P&lt;C"4&gt;7*T&gt;(*B&gt;'5?5%AY/C"1971A&lt;7^W:3"U&lt;S")&lt;WVF)&amp;"P=WFU;7^O%V")7$IA&lt;G6Y&gt;#"J&gt;'6S982J&lt;WY:1T%[)%.M;7.I:3"4:8*W;7.F)#UA;7ZJ&gt;"N$-DIA1WRJ9WBF)&amp;.F=H:J9W5A,3"B9X2J&gt;G5:1T-[)%.M;7.I:3"4:8*W;7.F)#UA:8BJ&gt;!B)4#"&amp;=H*P=AB-4#"&amp;=H*P=AV3:8.P&lt;(:F)%6S=G^S!!!/&lt;X*J:WFO97QA=86F&gt;75!!"R!1!!"`````Q!'$H*F9W6J&gt;G6E)(&amp;V:86F!!!@1!-!''.V=H*F&lt;H1A=X2F=!JB=H*B?3"J&lt;G2F?!!!#E!B"56S=G^S!!^!!Q!)=(*J&lt;H1A&lt;H)!!'-!]1!!!!!!!!!#&amp;&amp;"B:&amp;^1=GFO&gt;&amp;^W-V]R,GRW&lt;'FC&amp;WVB;7YA=(*P:S"J&lt;G2J9W&amp;U&lt;X)O9X2M!#Z!5!!+!!!!!1!#!!-!"!!&amp;!!=!#!!*!!I/&lt;7&amp;J&lt;C"J&lt;G2J9W&amp;U&lt;X)!!!%!#Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="time cliche" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!#!!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="time sub" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!#!!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Variable sensor" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typedefName1" Type="Str">Pad_Print_v3_1.lvlib:reading sensor.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../reading sensor.ctl</Property>
			<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#=G!!!!#!!A!!!!!!(!!B!)1*11A!!#%!B!E23!!!)1#%$2%*3!!B!)1.%1EQ!#%!B!E2-!!!-1#%'4'FH;(24!!"/!0%!!!!!!!!!!B21972@5(*J&lt;H2@&gt;D.@-3ZM&gt;GRJ9B*S:7&amp;E;7ZH)(.F&lt;H.P=CZD&gt;'Q!(E"1!!9!!!!"!!)!!Q!%!!5(=W6O=W^S=Q!"!!9!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Variable_graph" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typedefName1" Type="Str">Pad_Print_v3_1.lvlib:graph_variables.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../graph_variables.ctl</Property>
			<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*O;A)!!#!!A!!!!!!9!!F!#A!$5&amp;^9!!F!#A!$5&amp;^:!!F!#A!$6F^9!!F!#A!$6F^:!".!#A!.15EQ8U:P=G.F)&amp;.V9A!&lt;1!I!&amp;&amp;2P=C""9S"797R@2G^S9W5A6'^S!!!41!)!$62P=H&amp;V:3""1S"797Q!$U!$!!F18VB@:(*J&gt;G5!$U!$!!F18VF@:(*J&gt;G5!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!&amp;%"1!!-!#1!+!!M(28*S&lt;X)A?!!51&amp;!!!Q!*!!I!#Q&gt;&amp;=H*P=C":!!^!!Q!*6F^98W2S;8:F!!^!!Q!*6F^:8W2S;8:F!!^!#A!*6&amp;^98W2S;8:F!!^!#A!*6&amp;^:8W2S;8:F!!^!!Q!)=(*J&lt;H2@&lt;H)!!!V!#A!(&gt;'FN:6^N=Q!@1!-!'7.V=H*F&lt;H1A=X2B&gt;'5A98*S98EA;7ZE:8A!%5!+!!JM&lt;W&amp;E1W6M&lt;&amp;]R!!!21!I!#GRP972$:7RM8T)!!'M!]1!!!!!!!!!#&amp;&amp;"B:&amp;^1=GFO&gt;&amp;^W-V]R,GRW&lt;'FC%W&gt;S98"I8X:B=GFB9GRF=SZD&gt;'Q!/E"1!"1!!!!"!!)!!Q!%!!5!"A!(!!A!$!!.!!Y!$Q!1!"%!%A!4!"1!&amp;1!7"U.M&gt;8.U:8)!!1!8!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="variables" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typedefName1" Type="Str">Pad_Print_v3_1.lvlib:variables.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../variables.ctl</Property>
			<Property Name="typeDesc" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,0SQ)!!#!!A!!!!!!&lt;!"&amp;!#A!,7$-A2F"D&lt;'FD;'5!%U!+!!R77$-A2F:D&lt;'FD;'5!!!^!#A!)74)A2&amp;"Q971!!!F!#A!$6FES!!V!#A!'2#"1&gt;'^$!!!.1!I!"V2D&lt;'FD;'5!#5!+!!*:-1!!#5!+!!.774%!%5!+!!N7-#"#6G.M;7.I:1!01!I!#&amp;ET)%21='&amp;E!!!*1!I!!V::-Q!.1!I!"E%A5(2P5Q!!%5!+!!J15(*F=X.V=G5A!!!,1!I!"&amp;2T&gt;7)!!"&amp;!#A!,7$!A1F:D&lt;'FD;'5!$U!+!!B:-#"%5("B:!!!#5!+!!.774!!&amp;U!+!"&amp;M;7VJ&gt;#"Q&lt;X.J&gt;'FP&lt;C":-Q!41!I!$6"1=G6T=X6S:3!A1WQ!'U!+!"2M;7VJ&gt;#"Q&lt;X.J&gt;'FP&lt;C"$&lt;#":-A!!$U!+!!F1151A3'FH;(1!$5!+!!&gt;773"I&lt;WVF!!V!#A!(6FAA;'^N:1!01!I!#69A=W6S&gt;GFD:1!01!I!#6AA5W6S&gt;GFD:1!*1!I!!U&gt;B=!"T!0%!!!!!!!!!!B21972@5(*J&lt;H2@&gt;D.@-3ZM&gt;GRJ9AVW98*J97*M:8-O9X2M!%B!5!!;!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;A!8!"A!'1F798*J97*M:8-!!1!;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
	</Item>
	<Item Name="TypeDefs" Type="Folder">
		<Item Name="boolean_sensor_state.ctl" Type="VI" URL="../boolean_sensor_state.ctl"/>
		<Item Name="case_control.ctl" Type="VI" URL="../case_control.ctl"/>
		<Item Name="graph_variables.ctl" Type="VI" URL="../graph_variables.ctl"/>
		<Item Name="high_level_control.ctl" Type="VI" URL="../high_level_control.ctl"/>
		<Item Name="main prog cont switch.ctl" Type="VI" URL="../main prog cont switch.ctl"/>
		<Item Name="main prog indicator.ctl" Type="VI" URL="../main prog indicator.ctl"/>
		<Item Name="Module Configurations_self.ctl" Type="VI" URL="../Module Configurations_self.ctl"/>
		<Item Name="reading sensor.ctl" Type="VI" URL="../reading sensor.ctl"/>
		<Item Name="Shunt Calibration and Offset Nulling Measurements_self.ctl" Type="VI" URL="../Shunt Calibration and Offset Nulling Measurements_self.ctl"/>
		<Item Name="variables.ctl" Type="VI" URL="../variables.ctl"/>
	</Item>
	<Item Name="VIs" Type="Folder">
		<Item Name="actor_dequeue_state.vi" Type="VI" URL="../actor_dequeue_state.vi"/>
		<Item Name="actor_state_machine.vi" Type="VI" URL="../actor_state_machine.vi"/>
		<Item Name="axis_activate.vi" Type="VI" URL="../axis_activate.vi"/>
		<Item Name="build_measurement_cluster.vi" Type="VI" URL="../build_measurement_cluster.vi"/>
		<Item Name="Calculate Measurements (SubVI)_self.vi" Type="VI" URL="../Calculate Measurements (SubVI)_self.vi"/>
		<Item Name="calibrate force sensor.vi" Type="VI" URL="../calibrate force sensor.vi"/>
		<Item Name="check_boolean_sensors.vi" Type="VI" URL="../check_boolean_sensors.vi"/>
		<Item Name="create_actual_internal_values_array.vi" Type="VI" URL="../create_actual_internal_values_array.vi"/>
		<Item Name="create_FileName.vi" Type="VI" URL="../create_FileName.vi"/>
		<Item Name="enqueue_command.vi" Type="VI" URL="../enqueue_command.vi"/>
		<Item Name="find_systems.vi" Type="VI" URL="../find_systems.vi"/>
		<Item Name="FPGA_loadCell_Host_example.vi" Type="VI" URL="../FPGA_loadCell_Host_example.vi"/>
		<Item Name="Global.vi" Type="VI" URL="../Global.vi"/>
		<Item Name="loadCell_init.vi" Type="VI" URL="../loadCell_init.vi"/>
		<Item Name="loadCell_measurement.vi" Type="VI" URL="../loadCell_measurement.vi"/>
		<Item Name="Ratio to Microstrain (SubVI)_self.vi" Type="VI" URL="../Ratio to Microstrain (SubVI)_self.vi"/>
		<Item Name="read_axis_errors.vi" Type="VI" URL="../read_axis_errors.vi"/>
		<Item Name="read_boolean_sensors.vi" Type="VI" URL="../read_boolean_sensors.vi"/>
		<Item Name="Reinitialize.vi" Type="VI" URL="../Reinitialize.vi"/>
		<Item Name="Scale Measurments (SubVI)_self.vi" Type="VI" URL="../Scale Measurments (SubVI)_self.vi"/>
		<Item Name="scanEngine_Tests.vi" Type="VI" URL="../scanEngine_Tests.vi"/>
		<Item Name="ShuntCalibrationOutputCalculation (SubVI)_self.vi" Type="VI" URL="../ShuntCalibrationOutputCalculation (SubVI)_self.vi"/>
		<Item Name="TaF.vi" Type="VI" URL="../TaF.vi"/>
		<Item Name="Take Avg Measurement (SubVI)_self.vi" Type="VI" URL="../Take Avg Measurement (SubVI)_self.vi"/>
		<Item Name="Timer (SubVI)_self.vi" Type="VI" URL="../Timer (SubVI)_self.vi"/>
	</Item>
</Library>
